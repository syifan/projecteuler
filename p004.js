function largestPalindromicNumber() {
  'use strict';
  let max = 0;
  for (let i = 999; i >= 100; i--) {
    for (let j = 999; j >= i; j--) {
      let product = i * j;
      if (isPalindromic(product) && product > max) {
        max = product;
      }
    }
  }
  return max;
}

function isPalindromic(num) {
  'use strict';
  let arr = [];
  while (num > 0) {
    arr.push(num%10);
    num = Math.floor(num / 10);
  }
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== arr[arr.length - i - 1]) {
      return false;
    }
  }
  return true;
}

console.log(largestPalindromicNumber());
