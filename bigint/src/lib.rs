pub mod big_int {

    #[derive(Debug)]
    pub struct Int {
        digits: Vec<u8>,
    }

    impl Int {
        pub fn new() -> Int {
            Int { digits: vec![] }
        }

        pub fn from_string(s: &str) -> Int {
            let mut num = Int::new();

            for c in s.chars().rev() {
                num.digits.push(c.to_digit(10).unwrap() as u8)
            }

            return num;
        }

        pub fn add(a: &Int, b: &Int) -> Int {
            let mut res = Int::new();
            let mut i = 0;
            let mut carry = 0;

            loop {
                let mut da = 0;
                let mut db = 0;

                if i >= a.num_digit() && i >= b.num_digit() {
                    break;
                }

                if i < a.num_digit() {
                    da = a.digits[i];
                }

                if i < b.num_digit() {
                    db = b.digits[i]
                }

                let mut d = da + db + carry;
                carry = 0;
                if d >= 10 {
                    d -= 10;
                    carry = 1;
                }
                res.digits.push(d);

                // println!("{:?}, {}", res, carry);

                i += 1;
            }

            if carry > 0 {
                res.digits.push(1);
                // println!("{:?}, {}", res, carry);
            }

            return res;
        }

        pub fn num_digit(&self) -> usize {
            self.digits.len()
        }

        pub fn digits(&self) -> Vec<u8> {
            self.digits.to_vec()
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn add() {
            let a = Int::from_string("12");
            let b = Int::from_string("34");
            let c = Int::add(&a, &b);
            assert_eq!(c.digits[0], 6);
            assert_eq!(c.digits[1], 4);
        }

        #[test]
        fn add_with_carry() {
            let a = Int::from_string("99");
            let b = Int::from_string("1");
            let c = Int::add(&a, &b);
            assert_eq!(c.digits[0], 0);
            assert_eq!(c.digits[1], 0);
            assert_eq!(c.digits[2], 1);
        }
    }
}
