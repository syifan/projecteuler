fn main() {
    let mut longest_length = 0;
    let mut longest_number = 0;
    for i in 1..1000000 {
        let length = chain_length(i);
        // println!("{}: {}", i, length);
        if length > longest_length {
            longest_length = length;
            longest_number = i;
        }
    }
    println!("{}", longest_number);
}

fn chain_length(n: u64) -> u64 {
    let mut length = 1;
    let mut current = n;
    loop {
        match current {
            1 => {
                return length
            },
            _ => {
                match current%2 {
                    0 =>  {
                        current /= 2;
                        length += 1;
                    },
                    1 => {
                        current = 3 * current + 1;
                        length += 1;
                    },
                    _ => {},
                }
            }
        }
    }
}
