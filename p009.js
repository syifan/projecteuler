function specialPythagorean(sum) {
    'use strict';
    for (let i = 1; i < sum; i++) {
        for (let j = i; j < sum - i; j++) {
            let k = sum - i - j;
            if (i * i + j * j === k * k) {
                return [i, j, k]
            }
        }
    }
}

var numbers = specialPythagorean(1000);
console.log(numbers[0] * numbers[1] * numbers[2]);