let sum = 2;
let prev1 = 1;
let prev2 = 2;

while(true) {
  let num = prev1 + prev2;
  if (num >= 4000000) {
    break;
  }

  if (num % 2 === 0) {
    sum += num;
  }

  prev1 = prev2;
  prev2 = num;
}

console.log(sum);


