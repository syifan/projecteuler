function smallestMultiple() {
  'use strict';
  let number = 2;
  for (let i = 2; i <= 20; i++) {
    number = lcm(number, i);
  }
  return number;
}


function gcd(a, b) {
  'use strict';
  let t = 0;
  while(b!==0) {
    t = b;
    b = a % b;
    a = t;
  }
  return a;
}

function lcm(a, b) {
  'use strict';
  return a * b / gcd(a, b);
}
  
console.log(smallestMultiple());

