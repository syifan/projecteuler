function getTriangleNumber(index) {
	let number = 0;
	for (let i = 1; i <= index; i++) {
		number += i;
	}
	return number;
}

function getNumDivisor(number) {
	let numDivisor = 0;
	for (let i = 1; i <= number; i++) {
		if (number % i == 0) {
			numDivisor++;
		}
	}
	return numDivisor;
}

let i = 10000;
while(true) {
	i++;
	const triangleNumber = getTriangleNumber(i);
	console.log(i, triangleNumber);
	const numDivisor = getNumDivisor(triangleNumber);
	console.log(i, triangleNumber, numDivisor);
	if (numDivisor > 500) {
		console.log(triangleNumber);
		break;
	}
}
