function nthPrime(n) {
  'use strict';
  let primes = [];
  let nextNum = 2;
  while(primes.length < n) {
    let isPrime = true;
    for(let i = 0; i < primes.length; i++) {
      if (nextNum % primes[i] === 0) {
        isPrime = false;
        break;
      }
    } 

    if (isPrime) {
      primes.push(nextNum);
    }
    nextNum++;
  }

  return primes[n-1];
}

console.log(nthPrime(10001));
