let num = 600851475143;
let res = 0;
for (let i = 0; i < num; i++) {
  let f = num / i;
  if (!Number.isInteger(f)) {
    continue;
  }
  
  if (isPrime(f)) {
    res = f;
    break;
  }
}
console.log(res);

function isPrime(n) {
  'use strict';
  for (let f = 2; f < Math.floor(n/2); f++) {
    if (n % f === 0) {
      return false;
    }
  }
  return true;
}

