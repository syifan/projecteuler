fn main() {
    println!("{}", lattice2(20, 20))
}

fn lattice(n: u32, m: u32) -> u64 {
    if m == 0 || n == 0 {
        return 1;
    }

    lattice(n - 1, m) + lattice(n, m - 1)
}

fn lattice2(n: u32, m: u32) -> u64 {
    let mut results: Vec<Vec<u64>> = Vec::new();
    let mut i: usize = 0;
    let mut j: usize = 0;
    while i <= n as usize {
        results.push(Vec::new());
        j = 0;
        while j <= m as usize {
            if i == 0 || j == 0 {
                results[i].push(1);
            } else {
                let mut v: u64;
                {
                    v = results[i - 1][j] + results[i][j - 1];
                }
                results[i].push(v);
                // println!("({}, {}): {}", i, j, v);
            }
            j += 1;
        }
        i += 1;
    }

    // println!("{:?}", results);

    return results[n as usize][m as usize];
}
