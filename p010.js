function primeSum(max) {
    'use strict';
    let primes = [];
    let nextNum = 2;
    let sum = 0;
    while (nextNum < max) {
        let isPrime = true;
        for (let i = 0; i < primes.length; i++) {
            if (nextNum % primes[i] === 0) {
                isPrime = false;
                break;
            }
        }

        if (isPrime) {
            primes.push(nextNum);
            sum += nextNum;
        }
        nextNum++;

    }

    return sum;
}

console.log(primeSum(2000000));